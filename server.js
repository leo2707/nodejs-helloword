var express = require('express');
var app = express();
const chalk = require('chalk')

const port = process.env.PORT || 3000

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(port, () => {
  console.log(`${chalk.green('[helloword]')} server listening on port ${port}`)
})